module MyTrace where
import System.IO.Unsafe

myTrace :: IO a -> b -> b
myTrace action x = unsafePerformIO action `seq` x  

annotate :: String -> b -> b
annotate string = myTrace (putStrLn string) 

